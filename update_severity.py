#!/usr/bin/python

import csv
import datetime
import os
import sys

import gitlab

gl = gitlab.Gitlab('https://gitlab.freedesktop.org')
mesa = gl.projects.get(176)
known_issues = {}

class Issue:
    """
    Holds issue data for writing csv.
    """
    FIELDS = ['issue', 'severity', 'created', 'triaged', 'updated',
              'closed', 'author', 'culprit', 'assignee', 'labels',
              'flags', 'state', 'fixes']
    GITLAB_FIELDS = { 'iid': 'issue',
                      'created_at': 'created',
                      'closed_at': 'closed',
                      'updated_at': 'updated',
                     }

    def __init__(self, from_csv=None, from_gitlab=None):
        self.issue = None
        self.severity = None
        self.created = None
        self.triaged = None
        self.updated = None
        self.closed = None
        self.author = None
        self.culprit = None
        self.assignee = None
        self.labels = None
        self.flags = None
        self.state = None
        self.fixes = None

        if from_csv:
            for field in Issue.FIELDS:
                self.__dict__[field] = from_csv[field]
            return
        if from_gitlab:
            self.author = from_gitlab.author['username']
            self.assignee = None
            if from_gitlab.assignee:
                self.assignee = from_gitlab.assignee['username']
            from_gitlab.labels.sort()
            self.labels = ", ".join(from_gitlab.labels)
            for gitlab_field, issue_field in Issue.GITLAB_FIELDS.items():
                gitlab_value = None
                if gitlab_field in from_gitlab._attrs:
                    gitlab_value = from_gitlab._attrs[gitlab_field]
                self.__dict__[issue_field] = gitlab_value
            self.state = 'NeedsTriage'
            self.check_dont_care()
            if self.closed:
                self.state = 'Closed'

    def check_dont_care(self):
        if self.state != "NeedsTriage":
            return
        for dont_care in ["ACO", "AMD common", "CI", "CI alert", "CI daily",
                          "GLVND", "ISL", "Needs Information", "Not Our Bug",
                          "OpenCL", "Project Access Requests", "RADV", "TC", "To Do",
                          "VA-API", "VDPAU", "Windows", "asahi", "bifrost", "big endian",
                          "clover", "coverity", "crocus", "d3d12", "difficulty: easy",
                          "difficulty: hard", "difficulty: medium", "docs",
                          "dozen", "drisw", "enhancement", "etnaviv", "feature",
                          "freedreno", "good-first-task", "haiku", "i915g", "incident",
                          "ir3", "isaspec", "kopper", "labels", "lavapipe", "lima",
                          "llvmpipe", "loader", "macOS", "maintainer-scripts", "mapi",
                          "mediump", "midgard", "nine", "nouveau", "nv-vieux", "nv30",
                          "nv50", "nvc0", "osmesa", "panfrost", "panvk", "perfetto",
                          "question", "r100", "r200", "r300", "r600", "radeonsi",
                          "softpipe", "svga", "swr", "swrast", "tegra", "tracker",
                          "turnip", "v3d", "v3dv", "vbo", "vc4", "venus", "virgl",
                          "vmwgfx", "xfail", "zink"]:
            if dont_care in self.labels:
                self.state = "NotOurBug"
                self.flags = "NotOurBug"
                self.severity = "6"
                self.triaged = datetime.date.today().strftime("%Y-%m-%d")

    def update(self, _gitlab_issue):
        """update fields in the issue based on changes to the gitlab issue"""
        new_revision = Issue(from_gitlab=_gitlab_issue)
        for field in Issue.FIELDS:
            if field in ['severity', 'triaged', 'culprit', 'state']:
                continue
            self.__dict__[field] = new_revision.__dict__[field]
        self.check_dont_care()
        if self.closed:
            self.state = 'Closed'

parent_dir = os.path.dirname(os.path.abspath(sys.argv[0]))

# parse known issues
last_update = datetime.datetime.fromisoformat("2010-01-01T00:00:00.000+00:00")
with open(f'{parent_dir}/severity.csv', encoding='utf-8') as fh:
    reader = csv.DictReader(fh, delimiter='\t')
    for row in reader:
        issue = Issue(from_csv=row)
        known_issues[int(issue.issue)] = issue
        updated = datetime.datetime.fromisoformat(issue.updated.replace('Z', '+00:00'))
        if updated > last_update:
            last_update = updated

# obtain the last updated starting date

# iterate issues changed since last update

open_issues = mesa.issues.list(all=True, updated_after=last_update)
for gitlab_issue in open_issues:
    iid = gitlab_issue.iid
    if iid not in known_issues:
        issue = Issue(from_gitlab = gitlab_issue)
        known_issues[int(issue.issue)] = issue
    else:
        known_issues[int(iid)].update(gitlab_issue)

# iterate MRs modified after the previous update

# update Issues which have an unreviewed MR
mergerequests = mesa.mergerequests.list(all=True, updated_after=last_update)
for merge in mergerequests:
    if merge.state == 'closed':
        continue
    merge_id = str(merge.iid)
    for issue in merge.closes_issues():
        issue_id = issue.iid
        issue = known_issues[issue_id]
        if issue.state != 'Closed':
            # this issue has an unreviewed fix, and triage must be
            # done to assign to a reviewer.
            issue.state = "NeedsTriage"
        if not issue.fixes:
            issue.fixes = merge_id
        elif merge_id not in issue.fixes:
            issue.fixes = f"{issue.fixes}, {merge_id}"

with open(f'{parent_dir}/severity.csv', "w", encoding='utf-8') as fh:
    writer = csv.DictWriter(fh, delimiter='\t',
                            fieldnames=['issue', 'severity', 'created', 'triaged', 'updated',
                                        'closed', 'author', 'culprit', 'assignee', 'labels',
                                        'flags', 'state', 'fixes'])
    writer.writeheader()
    # sort issues
    issue_nums = sorted(known_issues.keys())
    issue_nums.reverse()
    for k in issue_nums:
        writer.writerow(known_issues[k].__dict__)

# write the time of the current update
